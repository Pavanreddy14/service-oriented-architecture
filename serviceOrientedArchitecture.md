# Service Oriented Architecture


### What is Service-Oriented Architecture?

*Service-Oriented Architecture (SOA) is a style of software design where services are provided to the other components by application components, through a communication protocol over a network.*

![Image of SOA](https://miro.medium.com/max/2348/1*Xot9nbkQAGbGaYwi84Kh-w.png)

*In similar words lets say we have two software one is service consumer and another one is a service provider. Now consumer wants to talk with the provider, so the consumer software is going to give a service request as a message to provider software. Then the provider is going to reply back with a service response .*

*When the request is received by the service provider software. It is processed by a service*

![Image of communication of two software](https://3.bp.blogspot.com/-Khrp72D2Twg/WMVgrJBPJhI/AAAAAAAAARg/74fnmbvXTBwg34z_WRhHE4cAHYSCvX27ACLcB/s1600/1.png)

### What is a service?

*A service is a well defined function that does not depend on the state of other services.*

*The Consumer software needs to know how to call this service eg: what argument or parameter the service excepting and also it needs to know what kind of response this service would be sending back to the consumer .*

*Now it's important to know that SOA is solution for making two software communicate with each other eg: when we want to fill out a webpage form you can easily do that On one side human and another side we have a software but when two software talk to each other there as to be a discipline in place. So that is where **Service-Oriented Architecture** comes into the picture .*
*This Implementation can be used for any form of web services .*

### Characteristics Of Service-Oriented Architecture

- SOA supports loose coupling everywhere in the project.

- SOA supports interoperability.

- SOA increases the quality of service

- SOA supports vendor diversity.

- SOA promotes discovery and federation.

- SOA is location-transparent

- SOA is still maturing and achievable idea

### Service-Oriented Architecture Patterns

![Image of SOA](https://miro.medium.com/max/560/1*CdJSSwWglOA1TWp-FwM9OQ.jpeg)

*There are three roles in each of the Service-Oriented Architecture building blocks: service provider; service broker, service registry, service repository; and service requester/consumer.*

- **Service provider** :*The service provider is a software entity that implements the services and also acts as the maintainer of the services. It provides the information of the available services by publishing them in the registry along with the details like contract specifications, nature, a procedure to handle, etc. So, the user can make use of it.*

- **Service Registry** : *An SOA Service Registry is the main component of SOA Governance. The Service registry allows service providers to efficiently find and connect with consumers, thereby acting as a bridge between service providers and service customers. The main focus of Service Registry is to provide fast and easy access to communication. It also wants to operate among different applications with minimal human intervention.*

- **Service Consumer** : *A service consumer is a software entity that requires services. Hence, it requests the services it needs by finding them in the registry and then binds and executes the service via the service provider. Generally, it can be another service or an end-user application.*

### Implementing Service-Oriented Architecture

*Depending on our needs, SOA can be implemented using many technologies, but they are mostly implemented with web services. One such example of a web service is SOAP. SOAP means  Simple Object Access Protocol is a messaging protocol that provides specifications for transferring structured information in computer networks. It came into use widely in the year 2003. Other ways to implement SOA are   Jini, COBRA, or REST. It can also be implemented in such a way where it acts independently for specific technologies that include messaging such as  ActiveMQ; Apache Thrift; and SORCER.*

### Benefits of Service-Oriented Architecture

![](https://miro.medium.com/max/610/1*QpkU690MioKK7lHwLE0_Bg.png)

*Various benefits of SOA are as follows*

*It allows code reusability which in turn saves the time spent on the development process, as the new service or process can be created using the reusable code present. Multiple coding languages can be used in so as everything runs in the central interface. *

*It promotes interaction by providing a standard form of communication that allows various systems and platforms to function independently. With this SOA can work around firewalls, thereby allowing companies to share services that are important for operations.*

*Scalability is an important feature that is required to meet the needs of the business or client. SOA provides this by reducing client-service interaction.*

*Cost reduction is done using SOA without comprising the quality of the level of output needed to be obtained. It limits the analysis required in developing custom solutions.*

### Reference:

[Source1](https://en.wikipedia.org/wiki/Service-oriented_architecture#Patterns)

[Source2](https://medium.com/@SoftwareDevelopmentCommunity/what-is-service-oriented-architecture-fa894d11a7ec)
